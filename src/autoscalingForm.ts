/* export var min: number = 0;
export var max: number = 0; */
export var formData = {
  min: 0,
  max: 0
};

export var min: number = 0;


export const validate = (min: number, max: number) => {
  if (min < 0) {
    return false;
  }
  if (max < 0) {
    return false;
  }
  if (max < min) {
    return false;
  }

  return true;
}

export const validate2 = () => {
  if (formData.min < 0) {
    return false;
  }
  if (formData.max < 0) {
    return false;
  }
  if (formData.max < formData.min) {
    return false;
  }

  return true;
}