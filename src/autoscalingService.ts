import axios from 'axios';

export const deployAutoscalingApp = async (min: number, max: number): Promise<any> => {
  return axios.post('http://localhost:3001/autoscaling', { min: min, max: max});
}