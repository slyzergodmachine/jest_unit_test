import { validate, validate2, formData } from '../src/autoscalingForm';
import * as autoscalingForm from '../src/autoscalingForm';

// Module
describe('Auto scaling form', () => {
  // Function
  describe('validate', () => {
    test('When min is a positive number, it should return true', () => {
      // Arrange
      // Act
      const result = validate(3, 4);
      // Assert
      expect(result).toBe(true);
    });
    test('When min is a negative number, it should return false', () => {
      // Arrange
      // Act
      const result = validate(-1, 1);
      // Assert
      expect(result).toBe(false);
    });

    test('When max is a positive number, it should return true', () => {
      // Arrange
      // Act
      const result = validate(3, 4);
      // Assert
      expect(result).toBe(true);
    });
    test('When max is a negative number, it should return false', () => {
      // Arrange
      // Act
      const result = validate(3, -1);
      // Assert
      expect(result).toBe(false);
    });

    test('When max greater than min, it should return true', () => {
      // Arrange
      // Act
      const result = validate(1, 3);
      // Assert
      expect(result).toBe(true);
    });
    test('When max less than min, it should return false', () => {
      // Arrange
      // Act
      const result = validate(6, 3);
      // Assert
      expect(result).toBe(false);
    });
  });
});

test('test validate 2', () => {
  // Arrange
  // Setup values in autoscalingForm
  formData.min = 3;
  formData.max = 5;

  // Act
  const result = validate2();
  // autoscalingForm.min = 3;

  // Assert
  expect(result).toBe(true);
});