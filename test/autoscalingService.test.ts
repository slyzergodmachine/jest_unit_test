import { deployAutoscalingApp } from '../src/autoscalingService';
import axios from 'axios';

jest.mock('axios');

// Module
describe('AutoScalingService', () => {
  // Function
  describe('deployAutoscalingApp', () => {
    test('When min and max are specified, it should return resultCode 20000', async () => {
      // Arrange
      const deployAppResponseStub = {
        data: {
          "resultCode": "20000",
          "developerMessage": "Success"
        }
      };
      // Stub
      // (axios.post as jest.Mock).mockResolvedValue(deployAppResponseStub);
      (axios.post as jest.Mock).mockImplementation(() => {
        return Promise.resolve(deployAppResponseStub);
      });

      // Act
      const deployAppResponse = await deployAutoscalingApp(2, 2);

      // Assert
      expect(deployAppResponse.data.resultCode).toBe('20000');
    })
  });
});