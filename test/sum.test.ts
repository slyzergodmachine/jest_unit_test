import { calculateSum } from '../src/sum';


describe('Sum', () => {
  describe('calculate sum', () => {
    test('when add 1 + 2, then it should return 3', () => {
      // Arrange

      // Act
      const actual = calculateSum(1, 2);

      // Assert
      expect(actual).toBe(3);
    });
  });
})
/* test('adds 1 + 2 to equal 3', () => {
  const result = sum(1, 2);
  expect(result).toBe(3);
});

test('object assignment', () => {
  const data = {one: 1};
  data['two'] = 2;
  expect(data).toEqual({one: 1, two: 2});
}); */